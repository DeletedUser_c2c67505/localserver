﻿using Core.Utils;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Core.Device
{
    public class PrinterManager : IPrinterManager, IDisposable
    {
        private const string CupsPath = "/usr/sbin/cupsctl";
        private const string LpPath = "/usr/bin/lp";

        public DeviceInfo? GetDeviceInfo(string deviceName)
        {
            FullDeviceInfo info = new()
            {
                Type = DeviceType.Printer,
                ID = deviceName
            };

            int numDests = CupsNative.GetDests(out IntPtr dests);

            if (dests == IntPtr.Zero)
                throw new InvalidOperationException("Failed to get printer list");

            try
            {
                IntPtr destPtr = CupsNative.GetDest(deviceName.ToLower() == "default" ? null : deviceName, IntPtr.Zero, numDests, dests);

                if (destPtr == IntPtr.Zero)
                    return null;

                cups_dest_t dest = (cups_dest_t)Marshal.PtrToStructure(destPtr, typeof(cups_dest_t));

                info.ID = dest.name;

                info.Capabilities = CupsUtils.GetCapabilities(dest);

                info.Formats = CupsUtils.GetAvailableFormats(destPtr);

                return info;
            }
            finally
            {
                CupsNative.FreeDests(numDests, dests);
            }
        }

        public IEnumerable<DeviceInfo> GetDevices()
        {
            int numDests = CupsNative.GetDests(out IntPtr dests);

            if (dests == IntPtr.Zero)
                throw new InvalidOperationException("Failed to get printer list");

            try
            {
                List<DeviceInfo> devices = new List<DeviceInfo>();

                for (int i = 0; i < numDests; i++)
                {
                    IntPtr destPtr = new IntPtr(dests.ToInt64() + i * Marshal.SizeOf(typeof(cups_dest_t)));
                    cups_dest_t dest = (cups_dest_t)Marshal.PtrToStructure(destPtr, typeof(cups_dest_t));

                    devices.Add(new DeviceInfo { ID = dest.name, Type = DeviceType.Printer });
                }

                return devices;
            }
            finally
            {
                CupsNative.FreeDests(numDests, dests);
            }
        }

        public bool Print(string printerName, Stream file, PrintProfile printProfile)
        {
            if (!File.Exists(CupsPath))
                throw new Exception("CUPS is not installed");

            var file_path = Path.GetTempFileName();
            byte[] buffer = new byte[file.Length];
            file.Read(buffer);
            File.WriteAllBytes(file_path, buffer);

            if (printerName.ToLower() != "default")
                printerName = "-d " + printerName + " ";
            else printerName = string.Empty;

            List<string> settings = new List<string>();

            if (printProfile.PageSize != PageSize.None) settings.Add("-o media=" + printProfile.PageSize.Dimensions().ToString());
            if (printProfile.Duplex) settings.Add("-o sides=two-sided-long-edge");
            if (printProfile.Resolution != null) settings.Add("-o print-quality=" + (int)printProfile.Resolution);

            var printerSettings = settings.Count != 0 ?
                string.Join(" ", settings) + " " : "";

            Process print = new Process();
            print.StartInfo.FileName = LpPath;
            print.StartInfo.UseShellExecute = true;
            print.StartInfo.CreateNoWindow = true;
            print.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            print.StartInfo.Arguments = printerName + printerSettings + file_path;
            print.Start();

            while (!print.HasExited)
            {
                Thread.Sleep(50);
            }

            File.Delete(file_path);

            return true;
        }

        public void Dispose()
        {
        }
    }
}
