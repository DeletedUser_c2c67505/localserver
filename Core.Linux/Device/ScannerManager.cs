﻿namespace Core.Device
{
    public class ScannerManager : IScannerManager, IDisposable
    {
        public void Dispose()
        {
        }

        public FullDeviceInfo GetDeviceInfo(DeviceInfo deviceInfo)
        {
            throw new NotImplementedException();
        }

        public DeviceInfo? GetDeviceInfo(string deviceName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DeviceInfo> GetDevices()
        {
            throw new NotImplementedException();
        }

        public byte[] Scan(string deviceName, ScanProfile scanProfile, out string contentType)
        {
            throw new NotImplementedException();
        }
    }
}
