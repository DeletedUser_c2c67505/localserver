﻿using System.Runtime.InteropServices;

namespace Core.Utils
{
    internal static class CupsNative
    {
        [DllImport("libcups.so.2", EntryPoint = "cupsGetDests", CharSet = CharSet.Ansi)]
        public static extern int GetDests(out IntPtr dests);

        [DllImport("libcups.so.2", EntryPoint = "cupsFreeDests", CharSet = CharSet.Ansi)]
        public static extern void FreeDests(int num_dests, IntPtr dests);

        [DllImport("libcups.so.2", EntryPoint = "cupsGetOption", CharSet = CharSet.Ansi)]
        public static extern IntPtr GetOption(string name, int num_options, IntPtr options);

        [DllImport("libcups.so.2", EntryPoint = "cupsCopyDestInfo", CharSet = CharSet.Ansi)]
        public static extern IntPtr CopyDestInfo(IntPtr http, IntPtr dest);

        [DllImport("libcups.so.2", EntryPoint = "cupsGetDestMediaCount", CharSet = CharSet.Ansi)]
        public static extern int GetDestMediaCount(IntPtr http, IntPtr dest, IntPtr info, int flags);

        [DllImport("libcups.so.2", EntryPoint = "cupsGetDestMediaByIndex", CharSet = CharSet.Ansi)]
        public static extern bool GetDestMediaByIndex(IntPtr http, IntPtr dest, IntPtr info, int index, int flags, ref cups_size_t size);

        [DllImport("libcups.so.2", EntryPoint = "cupsGetDest", CharSet = CharSet.Ansi)]
        public static extern IntPtr GetDest(string name, IntPtr instance, int num_dests, IntPtr dests);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct cups_dest_t
    {
        private IntPtr _name;
        public IntPtr instance;
        public bool is_default;
        public int num_options;
        public IntPtr options;

        public string name => Marshal.PtrToStringAnsi(_name);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct cups_size_t
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        private char[] _media;
        public int width, length, bottom, left, right, top;

        public string media => new string(_media).TrimEnd('\0');
    }

    [Flags]
    internal enum CupsType
    {
        CUPS_PRINTER_LOCAL = 0x0000,
        CUPS_PRINTER_CLASS = 0x0001,
        CUPS_PRINTER_REMOTE = 0x0002,
        CUPS_PRINTER_BW = 0x0004,
        CUPS_PRINTER_COLOR = 0x0008,
        CUPS_PRINTER_DUPLEX = 0x0010,
        CUPS_PRINTER_STAPLE = 0x0020,
        CUPS_PRINTER_COPIES = 0x0040,
        CUPS_PRINTER_COLLATE = 0x0080,
        CUPS_PRINTER_PUNCH = 0x0100,
        CUPS_PRINTER_COVER = 0x0200,
        CUPS_PRINTER_BIND = 0x0400,
        CUPS_PRINTER_SORT = 0x0800,
        CUPS_PRINTER_SMALL = 0x1000,
        CUPS_PRINTER_MEDIUM = 0x2000,
        CUPS_PRINTER_LARGE = 0x4000,
        CUPS_PRINTER_VARIABLE = 0x8000,
        CUPS_PRINTER_IMPLICIT = 0x10000,
        CUPS_PRINTER_DEFAULT = 0x20000,
        CUPS_PRINTER_FAX = 0x40000,
        CUPS_PRINTER_REJECTING = 0x80000,
        CUPS_PRINTER_DELETE = 0x100000,
        CUPS_PRINTER_NOT_SHARED = 0x200000,
        CUPS_PRINTER_AUTHENTICATED = 0x400000,
        CUPS_PRINTER_COMMANDS = 0x800000,
        CUPS_PRINTER_DISCOVERED = 0x1000000,
        CUPS_PRINTER_SCANNER = 0x2000000,
        CUPS_PRINTER_MFP = 0x4000000,
        CUPS_PRINTER_3D = 0x8000000,
        CUPS_PRINTER_OPTIONS = 0x6fffc
    }
}
