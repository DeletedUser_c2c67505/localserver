﻿using Core.Device;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace Core.Utils
{
    internal class CupsUtils
    {
        public static Dictionary<string, bool> GetCapabilities(cups_dest_t dest)
        {
            Dictionary<string, bool> capabilities = new()
            {
                { "duplex", false },
                { "color", false }
            };

            IntPtr attr = CupsNative.GetOption("printer-type", dest.num_options, dest.options);
            if (attr != IntPtr.Zero)
            {
                string printer_type = Marshal.PtrToStringAnsi(attr);
                var type = (CupsType)int.Parse(printer_type);

                capabilities["duplex"] = type.HasFlag(CupsType.CUPS_PRINTER_DUPLEX);
                capabilities["color"] = type.HasFlag(CupsType.CUPS_PRINTER_COLOR);
            }

            return capabilities;
        }

        public static Dictionary<string, Dimensions> GetAvailableFormats(IntPtr dest)
        {
            Dictionary<string, Dimensions> formats = new Dictionary<string, Dimensions>();

            var info = CupsNative.CopyDestInfo(IntPtr.Zero, dest);

            var count = CupsNative.GetDestMediaCount(IntPtr.Zero, dest, info, 0x00);

            cups_size_t size = new cups_size_t();
            for (int i = 0; i < count; i++)
            {
                if (CupsNative.GetDestMediaByIndex(IntPtr.Zero, dest, info, i, 0x00, ref size))
                {
                    Regex regex = new Regex("(?<type>na|jis|iso|roc|om|custom)_(?<name>\\S+)_(?<width>.+)x(?<height>.+)(?<unit>\\w{2})");
                    var match = regex.Match(size.media);

                    var type = match.Groups["type"].Value;
                    var name = match.Groups["name"].Value;
                    var width = match.Groups["width"].Value;
                    var height = match.Groups["height"].Value;
                    var measure = match.Groups["unit"].Value;

                    type = type.ToUpper();
                    name = name.Substring(0, 1).ToUpper() + name.Substring(1, name.Length - 1);

                    formats.Add($"{name} ({type})", new Dimensions { Width = decimal.Parse(width), Height = decimal.Parse(height), Unit = 
                    (
                        measure.Equals("in") ? PageSizeUnit.Inch :
                        measure.Equals("mm") ? PageSizeUnit.Millimetre : PageSizeUnit.None
                    )});
                }
            }

            return formats;
        }
    }
}
