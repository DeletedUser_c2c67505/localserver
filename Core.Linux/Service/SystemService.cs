using Newtonsoft.Json.Linq;
using NLog;

namespace Core.Service
{
    public class SystemService : ISystemService, IDisposable
    {
        public const string ConfigPath = "/etc/localserver.conf";
        public const string BasePath = "/usr/local/share/localserver/";

        public event EventHandler? Shutdown;
        public event EventHandler? ConfigurationChanged;

        private readonly Logger _logger;

        public SystemService(Logger logger)
        {
            _logger = logger;
        }

        public void Dispose()
        {
        }

        public void UpdateConfiguration(ref Configuration config)
        {
            if (!File.Exists(ConfigPath))
                return;

            try
            {
                var json = File.ReadAllText(ConfigPath);

                JObject cfg = JObject.Parse(json);

                config.Host = cfg.ContainsKey("Host") ? cfg.Value<string>("Host") : config.Host;
                config.Port = cfg.ContainsKey("Port") ? cfg.Value<string>("Port") : config.Port;
            }
            catch (Exception ex) { _logger.Error(ex, "Error when updating configuration"); }
        }

        public void StartServices(Configuration config)
        {
            UpdateConfiguration(ref config);
        }
    }
}
