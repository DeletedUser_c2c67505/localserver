﻿using System.Diagnostics;
using System.Drawing.Printing;

namespace Core.Device
{
    public class PrinterManager : IPrinterManager, IDisposable
    {
        private const string SumatraPath = "C:\\Program Files (x86)\\SumatraPDF\\SumatraPDF.exe";

        private static Dictionary<string, bool> GetCapabilities(PrinterSettings device)
        {
            Dictionary<string, bool> capabilities = new()
            {
                { "duplex", device.CanDuplex },
                { "color", device.SupportsColor }
            };

            return capabilities;
        }

        private static Dictionary<string, Dimensions> GetAvailableFormats(PrinterSettings device)
        {
            Dictionary<string, Dimensions> formats = new();

            foreach (PaperSize format in device.PaperSizes)
            {
                formats.Add(format.PaperName, new Dimensions { Height = format.Height / 100.0m, Width = format.Width / 100.0m, Unit = PageSizeUnit.Inch });
            }

            return formats;
        }

        public DeviceInfo? GetDeviceInfo(string deviceName)
        {
            FullDeviceInfo info = new()
            {
                ID = deviceName,
                Type = DeviceType.Printer,
            };

            PrinterSettings printer = new();

            if (deviceName.ToLower() != "default")
                printer.PrinterName = info.ID;
            else
                info.ID = printer.PrinterName;

            if (!printer.IsValid)
                throw new PrintException("Device not found", 404);

            info.Capabilities = GetCapabilities(printer);

            info.Formats = GetAvailableFormats(printer);

            return info;
        }

        public IEnumerable<DeviceInfo> GetDevices()
        {
            List<DeviceInfo> devices = new();

            foreach (string printerName in PrinterSettings.InstalledPrinters)
            {
                devices.Add(new DeviceInfo
                {
                    ID = printerName,
                    Type = DeviceType.Printer
                });
            }

            if (devices.Count == 0)
                throw new PrintException("Supported devices not found", 404);

            return devices;
        }

        public bool Print(string printerName, Stream file, PrintProfile printProfile)
        {
            if (!File.Exists(SumatraPath))
                 throw new PrintException("SumatraPDF is not installed", 500);

            var file_path = Path.GetTempFileName();
            byte[] buffer = new byte[file.Length];
            file.Read(buffer);
            File.WriteAllBytes(file_path, buffer);
            
            if (printerName.ToLower() != "default")
                printerName = "-print-to \"" + printerName + "\"";
            else printerName = "-print-to-default";

            List<string> settings = new List<string>();

            if (printProfile.Duplex) settings.Add("duplex");
            if (printProfile.PageSize != PageSize.None) settings.Add("paper=" + printProfile.PageSize.Format());

            var printerSettings = 
                "-print-settings \"" +
                string.Join(",", settings) +
                "\"";

            Process print = new Process();
            print.StartInfo.FileName = SumatraPath;
            print.StartInfo.UseShellExecute = true;
            print.StartInfo.CreateNoWindow = true;
            print.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            print.StartInfo.Arguments = printerName + " " + printerSettings + " -silent -exit-when-done \"" + file_path + "\"";
            print.Start();

            while (!print.HasExited)
            {
                Thread.Sleep(50);
            }

            File.Delete(file_path);

            return true;
        }

        public void Dispose()
        {
        }
    }
}
