using static Core.Utils.Twain.TwainAPI;
using System.Text;
using System.Drawing.Imaging;
using Core.Utils;
using System.Drawing;
using Docnet.Core;
using Docnet.Core.Editors;

namespace Core.Device
{
    public class ScannerManager : IScannerManager, IDisposable
    {
        public ScannerManager()
        {
            DTWAIN_SysInitialize();
        }

        private Dictionary<string, bool> GetCapabilities(IntPtr device)
        {
            Dictionary<string, bool> capabilities = new Dictionary<string, bool>();

            capabilities.Add("duplex", DTWAIN_IsDuplexSupported(device));
            capabilities.Add("adf", DTWAIN_IsFeederSupported(device));

            return capabilities;
        }

        private Dictionary<string, Dimensions> GetAvailableFormats(IntPtr device)
        {
            Dictionary<string, Dimensions> formats = new Dictionary<string, Dimensions>();

            int count = DTWAIN_EnumPaperSizes(device, out IntPtr sizes);

            for (int i = 0; i < count; i++)
            {
                DTWAIN_ArrayGetAtLong(sizes, i, out int size);


                PageSize pageSize = (PageSize)size;
                var dim = pageSize.Dimensions();
                if (dim is not null)
                    formats.Add(pageSize.ToString(), pageSize.Dimensions());
            }

            return formats;
        }

        public DeviceInfo? GetDeviceInfo(string deviceName)
        {
            IntPtr device = DTWAIN_SelectSourceByName(deviceName);

            if (device == IntPtr.Zero)
                throw new ScanException("Device not found", 404);

            StringBuilder deviceProductName = new StringBuilder(256);
            DTWAIN_GetSourceProductName(device, deviceProductName, 255);

            FullDeviceInfo info = new FullDeviceInfo
            {
                ID = deviceProductName.ToString(),
                Type = DeviceType.Scanner,
                Capabilities = GetCapabilities(device),
                Formats = GetAvailableFormats(device)
            };

            return info;
        }

        public IEnumerable<DeviceInfo> GetDevices()
        {
            List<DeviceInfo> devices = new List<DeviceInfo>();

            DTWAIN_EnumSources(out IntPtr sources);

            int count = DTWAIN_ArrayGetCount(sources);

            for (int i = 0; i < count; i++)
            {
                DTWAIN_ArrayGetAt(sources, i, out IntPtr source);

                if (source == IntPtr.Zero)
                    continue;

                StringBuilder builder = new StringBuilder(256);

                _ = DTWAIN_GetSourceProductName(source, builder, 255);

                if (builder.ToString().StartsWith("WIA-"))
                    continue;

                devices.Add(new DeviceInfo
                {
                    ID = builder.ToString(),
                    Type = DeviceType.Scanner
                });
            }

            if (devices.Count == 0)
                throw new ScanException("Supported devices not found", 404);

            return devices;
        }

        public byte[] Scan(string deviceName, ScanProfile scanProfile, out string contentType)
        {
            contentType = string.Empty;

            List<Bitmap> acquires = new List<Bitmap>();

            IntPtr device = DTWAIN_SelectSourceByName(deviceName);

            if (device == IntPtr.Zero)
                throw new ScanException($"Scanner with name {deviceName} not found", 404);

            var exception = ConfigureScanner(device, scanProfile);
            if (exception is not null)
                throw exception;

            IntPtr acquireArray = DTWAIN_CreateAcquisitionArray();

            try
            {
                DTWAIN_SetTwainTimeout(100);
                DTWAIN_SetMaxRetryAttempts(device, 0);

                using (DialogRemover remover = new DialogRemover())
                {
                    _ = DTWAIN_AcquireNativeEx(device, (int)(scanProfile.PixelType ?? PixelType.Default), -1,
                        scanProfile.ShowGUI.HasValue ? scanProfile.ShowGUI.Value : false, 1, acquireArray, out int status);
                }

                int acqCount = DTWAIN_ArrayGetCount(acquireArray);

                if (acqCount <= 0)
                    throw new ScanException("No pages acquired", 500);

                for (int acqInd = 0; acqInd < acqCount; acqInd++)
                {
                    IntPtr dibArray = DTWAIN_GetAcquiredImageArray(acquireArray, acqInd);

                    int dibCount = DTWAIN_ArrayGetCount(dibArray);

                    if (dibCount <= 0)
                        break;

                    for (int dibInd = 0; dibInd < dibCount; dibInd++)
                    {
                        _ = DTWAIN_ArrayGetAt(dibArray, dibInd, out IntPtr dib);

                        if (dib == IntPtr.Zero)
                            break;

                        Bitmap scan = Image.FromHbitmap(DTWAIN_ConvertDIBToBitmap(dib, IntPtr.Zero), IntPtr.Zero);

                        acquires.Add(scan);
                    }
                }


            }
            finally
            {
                _ = DTWAIN_DestroyAcquisitionArray(acquireArray, 1);
            }

            if (acquires.Count == 0)
                throw new ScanException("No pages acquired", 500);
            else
            {
                var images = acquires.Select(x =>
                {
                    using var ms = new MemoryStream();
                    x.Save(ms, ImageFormat.Jpeg);

                    return new JpegImage
                    {
                        Bytes = ms.ToArray(),
                        Width = x.Width,
                        Height = x.Height
                    };
                });

                contentType = "application/pdf";
                return DocLib.Instance.JpegToPdf(images.ToArray());
            }
        }

        private ScanException? ConfigureScanner(IntPtr device, ScanProfile profile)
        {
            #region Indicator

            DTWAIN_EnableIndicator(device, 0);

            #endregion

            #region Scan Type

            if (profile.ScanType is not null)
            {
                switch (profile.ScanType)
                {
                    case ScanType.Glass:
                        _ = DTWAIN_EnableFeeder(device, 0);
                        _ = DTWAIN_EnableDuplex(device, 0);
                        break;
                    case ScanType.Feeder:
                        _ = DTWAIN_EnableDuplex(device, 0);

                        if (!DTWAIN_IsFeederSupported(device))
                            return new ScanException("Feeder is not supported", 400);

                        _ = DTWAIN_EnableFeeder(device);
                        break;
                    case ScanType.Duplex:
                        if (!DTWAIN_IsDuplexSupported(device))
                            return new ScanException("Duplex is not supported", 400);

                        _ = DTWAIN_EnableFeeder(device);
                        _ = DTWAIN_EnableDuplex(device);
                        break;
                    default:
                        break;
                }
            }

            #endregion

            #region Page Size

            if (profile.PageSize is not null)
            {
                if (!DTWAIN_IsPaperSizeSupported(device, (int)profile.PageSize))
                    return new ScanException($"Format {profile.PageSize} is not supported", 400);

                _ = DTWAIN_SetPaperSize(device, (int)profile.PageSize);
            }

            #endregion

            #region Resolution

            if (profile.Resolution is not null)
            {
                DTWAIN_SetResolution(device, profile.Resolution.Value);
            }

            #endregion

            #region BitDepth

            if (profile.BitDepth is not null)
            {
                DTWAIN_SetBitDepth(device, profile.BitDepth.Value);
            }

            #endregion

            #region Brightness

            if (profile.Brightness is not null)
                DTWAIN_SetBrightness(device, profile.Brightness.Value);

            #endregion

            #region Contrast

            if (profile.Contrast is not null)
                DTWAIN_SetContrast(device, profile.Contrast.Value);

            #endregion

            return null;
        }

        public void Dispose()
        {
            DTWAIN_SysDestroy();
        }
    }
}
