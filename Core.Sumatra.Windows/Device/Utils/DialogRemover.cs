﻿using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace Core.Utils
{
    class DialogRemover : IDisposable
    {
        private readonly CancellationTokenSource source;
        private readonly Process proc;
        private readonly Task removerTask;
        private readonly string[] exceptions = new string[] { "Settings", "Default", "Twain", ".NET", "GDI", "MSC", "Scan" };
        public DialogRemover()
        {
            source = new CancellationTokenSource();
            proc = Process.GetCurrentProcess();

            removerTask = Task.Factory.StartNew(async () =>
            {
                while (!source.Token.IsCancellationRequested)
                {
                    var handle = GetWindowHandle(x =>
                    {
                        var title = GetWindowText(x);
                        return exceptions.All(x => !title.Contains(x)) && !string.IsNullOrEmpty(title);
                    });
                    if (handle != IntPtr.Zero)
                    {
                        var title = GetWindowText(handle);
                        SendMessage(handle, 0x0010, IntPtr.Zero, IntPtr.Zero);
                    }

                    await Task.Delay(250);
                }
            }, source.Token);
        }

        private static string GetWindowText(IntPtr handle)
        {
            int length = GetWindowTextLength(handle);
            StringBuilder sb = new(length + 1);
            _ = GetWindowText(handle, sb, sb.Capacity);
            return sb.ToString();
        }

        private IntPtr GetWindowHandle(Predicate<IntPtr> target)
        {
            IntPtr handle = IntPtr.Zero;

            foreach (ProcessThread thread in proc.Threads)
            {
                EnumThreadWindows(thread.Id,
                    (hwnd, lParam) => { if (target(hwnd)) { handle = hwnd; return false; } return true; }, IntPtr.Zero);

                if (handle != IntPtr.Zero)
                    break;
            }

            return handle;
        }

        public void Dispose()
        {
            source.Cancel();
            proc.Dispose();
        }

        delegate bool EnumThreadDelegate(IntPtr hWnd, IntPtr lParam);
        [DllImport("user32.dll")]
        static extern bool EnumThreadWindows(int dwThreadId, EnumThreadDelegate lpfn,
            IntPtr lParam);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetWindowTextLength(IntPtr hWnd);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
    }
}