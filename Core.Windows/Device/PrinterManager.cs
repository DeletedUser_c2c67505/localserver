﻿using Docnet.Core.Models;
using Docnet.Core;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Docnet.Core.Exceptions;

namespace Core.Device
{
    public class PrinterManager : IPrinterManager, IDisposable
    {
        private static Dictionary<string, bool> GetCapabilities(PrinterSettings device)
        {
            Dictionary<string, bool> capabilities = new()
            {
                { "duplex", device.CanDuplex },
                { "color", device.SupportsColor }
            };

            return capabilities;
        }

        private static Dictionary<string, Dimensions> GetAvailableFormats(PrinterSettings device)
        {
            Dictionary<string, Dimensions> formats = new();

            foreach (PaperSize format in device.PaperSizes)
            {
                formats.Add(format.PaperName, new Dimensions { Height = format.Height / 100.0m, Width = format.Width / 100.0m, Unit = PageSizeUnit.Inch });
            }

            return formats;
        }

        public DeviceInfo GetDeviceInfo(string deviceName)
        {
            FullDeviceInfo info = new()
            {
                ID = deviceName,
                Type = DeviceType.Printer,
            };

            PrinterSettings printer = new();

            if (deviceName.ToLower() != "default")
                printer.PrinterName = info.ID;
            else
                info.ID = printer.PrinterName;

            if (!printer.IsValid)
                throw new PrintException("Device not found", 404);

            info.Capabilities = GetCapabilities(printer);

            info.Formats = GetAvailableFormats(printer);

            return info;
        }

        public IEnumerable<DeviceInfo> GetDevices()
        {
            List<DeviceInfo> devices = new();

            foreach (string printerName in PrinterSettings.InstalledPrinters)
            {
                devices.Add(new DeviceInfo
                {
                    ID = printerName,
                    Type = DeviceType.Printer
                });
            }

            if (devices.Count == 0)
                throw new PrintException("Supported devices not found", 404);

            return devices;
        }

        public bool Print(string printerName, Stream file, PrintProfile printProfile)
        {
            Bitmap[] pages = GetBitmaps(file);

            PrintDocument printDocument = new()
            {
                DocumentName = "something"
            };

            if (printerName.ToLower() != "default")
                printDocument.PrinterSettings.PrinterName = printerName;

            var exception = ConfigurePrinter(ref printDocument, printProfile);
            if (exception is not null)
                throw exception;

            int pageIndex = 0;

            if (printDocument.PrinterSettings.IsValid)
            {
                printDocument.PrintPage += (sender, e) =>
                {
                    if (printDocument.PrinterSettings.IsValid)
                    {
                        if (pages[pageIndex] is null)
                            throw new PrintException("Page " + (pageIndex + 1) + " was null", 500);

                        e.Graphics?.DrawImage(pages[pageIndex], Rectangle.FromLTRB(0, 0, e.PageBounds.Width, e.PageBounds.Height));
                        e.HasMorePages = pageIndex + 1 < pages.Length;
                        pageIndex++;
                    }
                };

                printDocument.Print();
            }

            return true;
        }

        private PrintException? ConfigurePrinter(ref PrintDocument printDoc, PrintProfile profile)
        {
            #region Paper Size

            if (profile.PageSize is not PageSize.None)
            {
                PaperSize? selectedSize = printDoc.PrinterSettings.PaperSizes.Cast<PaperSize>().SingleOrDefault(x => x.Kind.ToString() == profile.PageSize.ToString());

                if (selectedSize is null)
                    return new PrintException("Selected page size is not supported", 400);

                printDoc.DefaultPageSettings.PaperSize = selectedSize;
            }

            #endregion

            #region Resolution

            if (profile.Resolution is not null)
            {
                PrinterResolution? selectedResolution = printDoc.PrinterSettings.PrinterResolutions.Cast<PrinterResolution>().SingleOrDefault(x => x.Kind.ToString() == profile.Resolution.ToString());

                if (selectedResolution is null)
                    return new PrintException("Selected resolution is not supported", 400);

                printDoc.DefaultPageSettings.PrinterResolution = selectedResolution;
            }

            #endregion

            #region Duplex

            if (profile.Duplex)
            {
                if (printDoc.PrinterSettings.CanDuplex)
                    printDoc.PrinterSettings.Duplex = Duplex.Vertical;
                else
                    return new PrintException("Duplex is not supported", 400);
            }
            else
                printDoc.PrinterSettings.Duplex = Duplex.Simplex;

            #endregion

            return null;
        }

        private static Bitmap[] GetBitmaps(Stream pdfFile)
        {
            using var reader = new BinaryReader(pdfFile);
            byte[] pdf = reader.ReadBytes((int)pdfFile.Length);

            try
            {
                var docReader = DocLib.Instance.GetDocReader(pdf,
                    new PageDimensions(5000, 5000));

                var count = docReader.GetPageCount();
                List<Bitmap> pages = new List<Bitmap>(count);

                for (int page = 0; page < count; page++)
                {
                    using var pageReader = docReader.GetPageReader(page);

                    var rawBytes = pageReader.GetImage();

                    var width = pageReader.GetPageWidth();
                    var height = pageReader.GetPageHeight();

                    var characters = pageReader.GetCharacters();


                    var bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);

                    AddBytes(bmp, rawBytes);

                    pages.Add(bmp);

                    bmp.Save($"C:\\Development\\Images\\test_{page}.bmp", ImageFormat.Bmp);
                }

                return pages.ToArray();
            }
            catch (DocnetLoadDocumentException)
            {
                throw new PrintException("File not in PDF format or corrupted", 400);
            }
        }

        private static void AddBytes(Bitmap bmp, byte[] rawBytes)
        {
            for (int j = 0; j < rawBytes.Length; j += 4)
            {
                if (rawBytes[j + 3] != 0) continue;

                rawBytes[j] = byte.MaxValue;
                rawBytes[j + 1] = byte.MaxValue;
                rawBytes[j + 2] = byte.MaxValue;
                rawBytes[j + 3] = byte.MaxValue;
            }

            var rect = new Rectangle(0, 0, bmp.Width, bmp.Height);

            var bmpData = bmp.LockBits(rect, ImageLockMode.WriteOnly, bmp.PixelFormat);
            var pNative = bmpData.Scan0;

            Marshal.Copy(rawBytes, 0, pNative, rawBytes.Length);
            bmp.UnlockBits(bmpData);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
