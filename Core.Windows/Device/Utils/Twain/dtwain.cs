using System.Runtime.InteropServices;
using System.Text;

namespace Core.Utils.Twain
{
    [Flags]
    public enum DuplexType : int
    {
        DTWAIN_DX_NONE = 0,
        DTWAIN_DX_1PASSDUPLEX = 1 << 0,
        DTWAIN_DX_2PASSDUPLEX = 1 << 1,
    }

    public class TwainAPI
    {
        private const string DTWAIN_LIBRARY = "dtwain";

        #region Init/Destroy Twain

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr DTWAIN_SysInitialize();

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_SysDestroy();

        #endregion

        #region Capabilities

        #region Indicator

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_EnableIndicator(IntPtr Source, int bEnable = 1);

        #endregion

        #region Duplex

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_GetDuplexType(IntPtr pSource, out DuplexType pDupType);

        public static bool DTWAIN_IsDuplexSupported(IntPtr pSource)
        {
            DTWAIN_GetDuplexType(pSource, out DuplexType pDupType);
            return pDupType != 0;
        }

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_EnableDuplex(IntPtr pSource, int bEnable = 1);

        #endregion

        #region Feeder

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool DTWAIN_IsFeederSupported(IntPtr pSource);

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_EnableFeeder(IntPtr pSource, int bSet = 1);

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_EnableAutoFeed(IntPtr pSource, int bSet = 1);

        #endregion

        #region PaperSize

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_EnumPaperSizes(IntPtr pSource, out IntPtr pArray);

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool DTWAIN_IsPaperSizeSupported(IntPtr pSource, int PaperSize);

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_SetPaperSize(IntPtr pSource, int PaperSize, bool bSetCurrent = true);

        #endregion

        #region AutoDeskew

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool DTWAIN_IsAutoDeskewSupported(IntPtr pSource);

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_EnableAutoDeskew(IntPtr pSource, bool bEnable);

        #endregion

        #region Resolution

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_SetResolution(IntPtr pSource, double Resolution);

        #endregion

        #region BitDepth

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_SetBitDepth(IntPtr pSource, int BitDepth, bool bSetCurrent = true);

        #endregion

        #region Brightness

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_SetBrightness(IntPtr pSource, double Brightness);

        #endregion

        #region Contrast

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_SetContrast(IntPtr pSource, double Contrast);

        #endregion

        #region BlankPage Detection

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_SetBlankPageDetection(IntPtr pSource, double threshold, int autodetect_option, bool bSet = true);

        #endregion

        #region Custom

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_IsCapSupported(IntPtr pSource, int lCapability);

        #endregion

        #endregion

        #region Array

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr DTWAIN_CreateAcquisitionArray();

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_DestroyAcquisitionArray(IntPtr aAcq, int bDestroyData);

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_ArrayGetAt(IntPtr pArray, int nWhere, out IntPtr pVariant);

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_ArrayGetAtLong(IntPtr pArray, int nWhere, out int pVal);

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_ArrayGetCount(IntPtr pArray);

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr DTWAIN_GetAcquiredImageArray(IntPtr aAcq, int nWhichAcq);

        #endregion

        #region Source

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr DTWAIN_SelectSourceByName([MarshalAs(UnmanagedType.LPTStr)] string lpszName);

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_EnumSources(out IntPtr pArray);

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_GetSourceProductName(IntPtr pSource, [MarshalAs(UnmanagedType.LPTStr)] StringBuilder szProduct, int nMaxLen);

        #endregion

        #region Acquire

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_SetTwainTimeout(int milliseconds);
        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_SetMaxRetryAttempts(IntPtr Source, int nAttempts);

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern int DTWAIN_AcquireNativeEx(IntPtr pSource, int PixelType, int nMaxPages, bool bShowUI, int bCloseSource, IntPtr pAcq, out int pStatus);

        #endregion

        #region Image

        [DllImport(DTWAIN_LIBRARY, CharSet = CharSet.Auto,
        ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr DTWAIN_ConvertDIBToBitmap(IntPtr hDib, IntPtr hPalette);

        #endregion
    }
}
