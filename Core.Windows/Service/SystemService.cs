using Core.Service.Utils;
using Core.Windows.Service.Utils;
using Microsoft.Win32;
using NLog;
using System.Runtime.InteropServices;

namespace Core.Service
{
    public class SystemService : ISystemService, IDisposable
    {
        private TrayWorker _tray;

        public event EventHandler? Shutdown;
        public event EventHandler? ConfigurationChanged;

        public SystemService(Logger logger)
        {
            
        }

        public void Dispose()
        {
            if (_tray != null)
                _tray.Dispose();
        }

        public void UpdateConfiguration(ref Configuration config)
        {
            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Default))
            using (var key = hklm.OpenSubKey(@"SOFTWARE\MMK\LocalServer"))
            {
                if (key != null)
                {
                    config.Host = (string?)key.GetValue("Host") ?? config.Host;
                    config.Port = (string?)key.GetValue("Port") ?? config.Port;
                }
            }
        }

        public void StartServices(Configuration config)
        {
            if (config.Debug)
                AllocConsole();

            UpdateConfiguration(ref config);

            _tray = TrayWorker.Init(config.Host!, config.Port!);
            _tray.Shutdown += (s, e) => Shutdown?.Invoke(s, e);
            _tray.Settings += (s, e) =>
            {
                UpdateConfiguration(ref config);
                var dlg = new SettingsDlg(config);
                var result = dlg.ShowDialog();

                if (result == DialogResult.OK)
                {
                    UpdateConfiguration(ref config);

                    _tray.Update(config.Host!, config.Port!);
                    ConfigurationChanged?.Invoke(config, e);
                }
            };
        }

        [DllImport("kernel32")]
        static extern bool AllocConsole();
    }
}
