﻿namespace Core.Service.Utils
{
    partial class SettingsDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.host_comboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.port_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.saveButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.port_numericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Хост";
            // 
            // host_comboBox
            // 
            this.host_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.host_comboBox.FormattingEnabled = true;
            this.host_comboBox.Items.AddRange(new object[] {
            "localhost",
            "0.0.0.0"});
            this.host_comboBox.Location = new System.Drawing.Point(50, 12);
            this.host_comboBox.Name = "host_comboBox";
            this.host_comboBox.Size = new System.Drawing.Size(169, 23);
            this.host_comboBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Порт";
            // 
            // port_numericUpDown
            // 
            this.port_numericUpDown.Location = new System.Drawing.Point(50, 41);
            this.port_numericUpDown.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.port_numericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.port_numericUpDown.Name = "port_numericUpDown";
            this.port_numericUpDown.Size = new System.Drawing.Size(169, 23);
            this.port_numericUpDown.TabIndex = 4;
            this.port_numericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.port_numericUpDown.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.port_numericUpDown_KeyPress);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(144, 94);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 5;
            this.saveButton.Text = "Сохранить";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // SettingsDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 133);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.port_numericUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.host_comboBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            ((System.ComponentModel.ISupportInitialize)(this.port_numericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private ComboBox host_comboBox;
        private NumericUpDown numericUpDown1;
        private Label label2;
        private TextBox port_textBox;
        private NumericUpDown port_numericUpDown;
        private Button saveButton;
        private Button cancelButton;
    }
}