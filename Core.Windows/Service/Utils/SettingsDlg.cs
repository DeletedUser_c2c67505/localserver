﻿using Microsoft.Win32;

namespace Core.Service.Utils
{
    public partial class SettingsDlg : Form
    {
        private readonly Configuration _config;

        public SettingsDlg(Configuration config)
        {
            InitializeComponent();

            _config = config;

            host_comboBox.Text = config.Host;
            port_numericUpDown.Text = config.Port;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (_config.Host != host_comboBox.Text || _config.Port != port_numericUpDown.Text)
            {
                DialogResult = DialogResult.OK;

                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Default))
                using (var key = hklm.OpenSubKey(@"SOFTWARE\MMK\LocalServer", true))
                {
                    if (key != null)
                    {

                        key.SetValue("Host", host_comboBox.Text);
                        key.SetValue("Port", port_numericUpDown.Text);
                    }
                }
            }

            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void port_numericUpDown_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.' || e.KeyChar == ',') e.Handled = true;
        }
    }
}
