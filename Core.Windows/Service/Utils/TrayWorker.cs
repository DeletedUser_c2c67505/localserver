﻿namespace Core.Windows.Service.Utils
{
    public class TrayWorker : IDisposable
    {
        private Task _trayTask;
        private NotifyIcon _tray;
        private ContextMenuStrip _context;

        private TrayWorker(string Host, string Port)
        {
            _trayTask = Task.Factory.StartNew(() =>
            {
                _tray = new NotifyIcon
                {
                    Icon = Properties.Resources.server
                };

                _context = new ContextMenuStrip();
                _context.SuspendLayout();

                _context.Items.AddRange(new ToolStripItem[] {
                    new ToolStripMenuItem
                    {
                        Name = "info-host",
                        Text = $"Host: {Host}",
                        Enabled = false
                    },
                    new ToolStripMenuItem
                    {
                        Name = "info-port",
                        Text = $"Port: {Port}",
                        Enabled = false
                    },
                    new ToolStripMenuItem
                    {
                        Name = "settings",
                        Text = "Настройки"
                    },
                    new ToolStripMenuItem
                    {
                        Name = "close",
                        Text = "Закрыть"
                    }
                });

                _context.Items["settings"].Click += (s, e) => Settings?.Invoke(_tray, EventArgs.Empty);
                _context.Items["close"].Click += (s, e) =>
                {
                    _tray.Visible = false;
                    Shutdown?.Invoke(_tray, EventArgs.Empty);
                };

                _context.ResumeLayout(false);
                _tray.ContextMenuStrip = _context;

                _tray.Visible = true;

                Application.Run();
            });
        }

        public event EventHandler Settings;
        public event EventHandler Shutdown;

        public static TrayWorker Init(string Host, string Port)
        {
            return new TrayWorker(Host, Port);
        }
        public void Update(string Host, string Port)
        {
            _context.Items["info-host"].Text = $"Host: {Host}";
            _context.Items["info-port"].Text = $"Port: {Port}";
        }

        public void Dispose()
        {
            if (!_trayTask.IsCompleted)
                _trayTask.Dispose();

            if (_context != null)
                _context.Dispose();

            if (_tray != null)
                _tray.Dispose();
        }
    }
}
