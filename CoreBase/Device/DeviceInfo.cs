﻿namespace Core.Device
{
    public enum DeviceType
    {
        Printer,
        Scanner
    }

    public class DeviceInfo
    {
        public string ID { get; set; }

        public DeviceType Type { get; set; }
    }

    public class FullDeviceInfo : DeviceInfo
    {
        public Dictionary<string, bool> Capabilities { get; set; }

        public Dictionary<string, Dimensions> Formats { get; set; }
    }
}
