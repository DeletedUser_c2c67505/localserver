﻿namespace Core.Device
{
    public interface IDeviceManager
    {
        public abstract IEnumerable<DeviceInfo> GetDevices();

        public abstract DeviceInfo? GetDeviceInfo(string deviceName);
    }
}
