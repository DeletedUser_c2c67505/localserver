﻿namespace Core.Device
{
    public interface IPrinterManager : IDeviceManager
    {
        public abstract bool Print(string printerName, Stream file, PrintProfile printProfile);
    }
}
