﻿namespace Core.Device
{
    public interface IScannerManager : IDeviceManager
    {
        public abstract byte[] Scan(string deviceName, ScanProfile scanProfile, out string contentType);
    }
}
