using System.Globalization;

namespace Core.Device
{
    public enum PageSize
    {
        None = 0,

        [Format("A4")]
        [Dimensions("210", "297", PageSizeUnit.Millimetre)]
        A4 = 1,
        [Format("letter")]
        [Dimensions("8.5", "11", PageSizeUnit.Inch)]
        Letter = 3,
        [Dimensions("8.5", "14", PageSizeUnit.Inch)]
        [Format("legal")]
        Legal = 4,
        [Dimensions("148", "210", PageSizeUnit.Millimetre)]
        [Format("A5")]
        A5 = 5,
        [Dimensions("297", "420", PageSizeUnit.Millimetre)]
        [Format("A3")]
        A3 = 11,
        [Dimensions("105", "148", PageSizeUnit.Millimetre)]
        [Format("A6")]
        A6 = 13,
        [Dimensions("420", "594", PageSizeUnit.Millimetre)]
        [Format("A2")]
        A2 = 21,
        [Dimensions("5.5", "8.5", PageSizeUnit.Inch)]
        [Format("statement")]
        Statement = 52
    }

    [Serializable]
    public class Dimensions
    {
        public decimal Width { get; set; }

        public decimal Height { get; set; }

        public PageSizeUnit Unit { get; set; }

        public override string ToString()
        {
            return $"Custom.{Width}x{Height}{Unit.Format()}";
        }

        public override int GetHashCode()
        {
            return Width.GetHashCode() ^ Height.GetHashCode() ^ Unit.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is Dimensions pageDimens && this == pageDimens;
        }

        public static bool operator ==(Dimensions x, Dimensions y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
            {
                return false;
            }
            return x.Width == y.Width && x.Height == y.Height && x.Unit == y.Unit;
        }

        public static bool operator !=(Dimensions x, Dimensions y) => !(x == y);
    }

    public class DimensionsAttribute : Attribute
    {
        public DimensionsAttribute(string width, string height, PageSizeUnit unit)
        {
            PageDimensions = new Dimensions
            {
                Width = decimal.Parse(width, CultureInfo.InvariantCulture),
                Height = decimal.Parse(height, CultureInfo.InvariantCulture),
                Unit = unit
            };
        }

        public Dimensions PageDimensions { get; }
    }

    public class FormatAttribute : Attribute
    {
        public FormatAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }

    public class UnitAttribute : Attribute
    {
        public UnitAttribute(string name)
        {
            this.String = name;
        }

        public string String { get; }
    }

    public enum PageSizeUnit
    {
        [Unit("")]
        None,
        [Unit("in")]
        Inch,
        [Unit("cm")]
        Centimetre,
        [Unit("mm")]
        Millimetre
    }

    public static class EnumExtensions
    {
        public static Dimensions Dimensions(this PageSize enumValue)
        {
            object[] attrs = enumValue.GetType().GetField(enumValue.ToString()).GetCustomAttributes(typeof(DimensionsAttribute), false);
            return attrs.Cast<DimensionsAttribute>().Select(x => x.PageDimensions).SingleOrDefault();
        }

        public static string Format(this PageSize enumValue)
        {
            object[] attrs = enumValue.GetType().GetField(enumValue.ToString()).GetCustomAttributes(typeof(FormatAttribute), false);
            return attrs.Cast<FormatAttribute>().Select(x => x.Name).SingleOrDefault();
        }

        public static string Format(this PageSizeUnit enumValue)
        {
            object[] attrs = enumValue.GetType().GetField(enumValue.ToString()).GetCustomAttributes(typeof(UnitAttribute), false);
            return attrs.Cast<UnitAttribute>().Select(x => x.String).SingleOrDefault();
        }

        public static decimal WidthInInches(this Dimensions pageDimensions)
        {
            switch (pageDimensions.Unit)
            {
                case PageSizeUnit.Inch:
                    return pageDimensions.Width;
                case PageSizeUnit.Centimetre:
                    return pageDimensions.Width * 0.393701m;
                case PageSizeUnit.Millimetre:
                    return pageDimensions.Width * 0.0393701m;
                default:
                    throw new ArgumentException();
            }
        }

        public static decimal HeightInInches(this Dimensions pageDimensions)
        {
            switch (pageDimensions.Unit)
            {
                case PageSizeUnit.Inch:
                    return pageDimensions.Height;
                case PageSizeUnit.Centimetre:
                    return pageDimensions.Height * 0.393701m;
                case PageSizeUnit.Millimetre:
                    return pageDimensions.Height * 0.0393701m;
                default:
                    throw new ArgumentException();
            }
        }
    }
}
