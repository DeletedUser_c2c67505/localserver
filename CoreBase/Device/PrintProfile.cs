﻿using System.Runtime.Serialization;

namespace Core.Device
{
    public class PrintProfile
    {
        public PageSize PageSize { get; set; } = Device.PageSize.None;
        public Resolution? Resolution { get; set; }
        public bool Duplex { get; set; }
    }

    public enum Resolution
    {
        High = 5,
        Medium = 4,
        Low = 3,
        Draft = 3
    }

    [Serializable]
    public class PrintException : Exception
    {
        public PrintException(int statusCode) { StatusCode = statusCode; }
        public PrintException(string message, int statusCode) : base(message) { StatusCode = statusCode; }
        public PrintException(string message, int statusCode, Exception inner) : base(message, inner) { StatusCode = statusCode; }
        protected PrintException(
          SerializationInfo info,
          StreamingContext context) : base(info, context) { }

        public int StatusCode;
    }
}
