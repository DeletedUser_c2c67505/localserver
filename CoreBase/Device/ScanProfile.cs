﻿using System.Runtime.Serialization;

namespace Core.Device
{
    public class ScanProfile
    {
        public ScanType? ScanType { get; set; }
        public PixelType? PixelType { get; set; }
        public PageSize? PageSize { get; set; }
        public double? Resolution { get; set; }
        public int? BitDepth { get; set; }
        public double? Brightness { get; set; }
        public double? Contrast { get; set; }
        public bool? ShowGUI { get; set; }
    }

    public enum ScanType
    {
        Glass,
        Feeder,
        Duplex
    }

    public enum PixelType
    {
        Default = 1000,
        WhiteBlack = 0,
        GrayScale = 1,
        Color = 2,
    }

    [Serializable]
    public class ScanException : Exception
    {
        public ScanException(int statusCode) { StatusCode = statusCode; }
        public ScanException(string message, int statusCode) : base(message) { StatusCode = statusCode; }
        public ScanException(string message, int statusCode, Exception inner) : base(message, inner) { StatusCode = statusCode; }
        protected ScanException(
          SerializationInfo info,
          StreamingContext context) : base(info, context) { }

        public int StatusCode;
    }
}
