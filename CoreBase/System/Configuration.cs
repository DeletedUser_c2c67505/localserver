﻿namespace Core.Service
{
    public struct Configuration
    {
        public bool Debug { get; set; }
        public string? Host { get; set; }
        public string? Port { get; set; }
    }
}
