﻿namespace Core.Service
{
    public interface ISystemService
    {
        event EventHandler Shutdown;
        event EventHandler ConfigurationChanged;

        void StartServices(Configuration config);
        void UpdateConfiguration(ref Configuration config);
    }
}
