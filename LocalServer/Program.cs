using Core.Service;
using LocalServer;
using Microsoft.Extensions.Options;
using NLog;
using NLog.Web;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Reflection;
using System.Text.Json.Serialization;

internal class Program
{
    private static WebApplication? _app = null;
    private static Logger? _logger;
    private static bool _restartRequest = true;
    private static Configuration _config = new Configuration { Debug = false, Host = "localhost", Port = "5000" };

    private static async Task Main(string[] args)
    {
        foreach (var arg in args)
        {
            switch (arg.ToLower())
            {
                case "-d":
                case "--debug":
                    _config.Debug = true;
                    break;
                default:
                    break;
            }

        }

        _logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();

        LogManager.Configuration.Variables["minlevel"] = _config.Debug ? "Debug" : "Info";
        LogManager.ReconfigExistingLoggers();

        ISystemService systemService = new SystemService(_logger);

        try
        {
            systemService.StartServices(_config);
        }
        catch (Exception ex)
        {
            _logger.Error(ex, "Stopped program because of exception");
            throw;
        }

        _logger.Debug("init main");

        systemService.Shutdown += async (s, e) => await _app!.StopAsync();
        systemService.ConfigurationChanged += async (s, e) =>
        {
            _restartRequest = true;
            await _app!.StopAsync();
        };

        try
        {
            while (_restartRequest)
            {
                _restartRequest = false;

                systemService.UpdateConfiguration(ref _config);

                var builder = CreateBuilder(_config);
                _app = CreateServer(builder);
                await _app.RunAsync();
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex, "Stopped program because of exception");
            throw;
        }
    }

    private static WebApplicationBuilder CreateBuilder(Configuration config)
    {
        try
        {
            var builder = WebApplication.CreateBuilder();

            builder.WebHost.UseUrls($"http://{config.Host}:{config.Port}/");

            builder.Logging.ClearProviders();
            builder.Host.UseNLog();

            builder.Services.AddCors(options =>
            {
                var origins = builder.Configuration.GetSection("AllowedOrigins").Get<string[]>();

                options.AddDefaultPolicy(config =>
                    config.AllowAnyMethod()
                           .AllowAnyHeader()
                           .WithOrigins(origins));
            });

            builder.Services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                });

            builder.Services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
            })
            .AddMvc()
            .AddApiExplorer(options =>
            {
                options.GroupNameFormat = "'API v'VVV";

                options.SubstituteApiVersionInUrl = true;
            });

            builder.Services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

            if (_config.Debug)
            {
                builder.Services.AddSwaggerGen(options =>
                {
                    options.OperationFilter<SwaggerDefaultValues>();

                    var fileName = typeof(Program).Assembly.GetName().Name + ".xml";
                    var filePath = Path.Combine(AppContext.BaseDirectory, fileName);

                    options.IncludeXmlComments(filePath);
                });
            }
            builder.Services.AddStackPolicy(options =>
            {
                options.MaxConcurrentRequests = 1;
                options.RequestQueueLimit = 25;
            });

            return builder;
        }
        catch (Exception ex)
        {
            _logger?.Error(ex, "Stopped program because of exception");
            throw;
        }
    }
    private static WebApplication CreateServer(WebApplicationBuilder builder)
    {
        try
        {
            var app = builder.Build();

            if (_config.Debug)
            {
                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {
                    var descriptions = app.DescribeApiVersions();

                    foreach (var description in descriptions)
                    {
                        var url = $"/swagger/{description.GroupName}/swagger.yaml";
                        var name = description.GroupName.ToUpperInvariant();
                        options.SwaggerEndpoint(url, name);
                    }
                });
            }

            app.MapControllers();
            app.UseConcurrencyLimiter();

            app.UseCors();

            return app;
        }
        catch (Exception ex)
        {
            _logger?.Error(ex, "Stopped program because of exception");
            throw;
        }
    }
}