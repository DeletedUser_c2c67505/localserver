﻿using Asp.Versioning;
using Core.Device;
using Microsoft.AspNetCore.Mvc;

namespace LocalServer.V1.Controllers
{
    /// <summary>
    /// Контроллер для работы с принтерами
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("device/v{version:apiVersion}/[controller]")]
    public class PrinterController : ControllerBase
    {
        private readonly ILogger<PrinterController> _logger;

        public PrinterController(ILogger<PrinterController> logger)
        {
            _logger = logger;
            _logger.LogDebug("NLog injected into PrinterController");
        }

        /// <summary>
        /// Список доступных принтеров.
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<DeviceInfo>), StatusCodes.Status200OK)]
        public virtual ActionResult<IEnumerable<DeviceInfo>> GetDevices()
        {
            _logger.LogDebug("GetDevices request recieved");

            try
            {
                using var manager = new PrinterManager();

                var devices = manager.GetDevices();

                _logger.LogDebug("GetDevices request completed");

                return Ok(devices);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Request failed because of exception");

                if (ex is PrintException pex)
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = pex.Message,
                        Status = pex.StatusCode
                    });
                else
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = ex.Message,
                        Status = 500
                    });
            }
        }

        /// <summary>
        /// Информация о выбранном принтере.
        /// </summary>
        /// <param name="id">Имя принтера</param>
        [HttpGet]
        [ProducesResponseType(typeof(FullDeviceInfo), StatusCodes.Status200OK)]
        [Route("{id}")]
        public virtual ActionResult<FullDeviceInfo> GetDeviceInfo(string id)
        {
            _logger.LogDebug("GetDeviceInfo request recieved with id: " + id);

            try
            {
                using var manager = new PrinterManager();

                var device = manager.GetDeviceInfo(id);

                _logger.LogDebug("GetDeviceInfo request completed");

                return Ok(device);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "GetDeviceInfo request failed because of exception");

                if (ex is PrintException pex)
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = pex.Message,
                        Status = pex.StatusCode
                    });
                else
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = ex.Message,
                        Status = 500
                    });
            }
        }

        /// <summary>
        /// Запрос на печать документа.
        /// </summary>
        /// <param name="id">Имя принтера (Default для выбора принтера по умолчанию)
        /// По умолчанию: Не указано</param>
        /// <param name="file">Документ в формате PDF</param>
        /// <param name="file_url">Ссылка на документ в формате PDF</param>
        /// <param name="format">Формат бумаги: A4, B3, Letter и тд</param>
        /// <param name="resolution">Разрешение печати: High, Medium, Low</param>
        /// <param name="duplex">Двусторонняя печать
        /// По умолчанию: Отключено</param>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("{id}/print")]
        public virtual ActionResult Print(string id, IFormFile? file, Uri? file_url,
            Resolution? resolution, PageSize format = PageSize.None, bool duplex = false)
        {
            _logger.LogDebug("Print request recieved with id: " + id);

            try
            {
                using var manager = new PrinterManager();

                Stream stream;

                if (file_url is not null)
                {
                    try
                    {
                        using var webClient = new HttpClient();
                        byte[] arr = webClient.GetByteArrayAsync(file_url).GetAwaiter().GetResult();
                        stream = new MemoryStream(arr);
                    }
                    catch (HttpRequestException)
                    {
                        return new ObjectResult(new ProblemDetails
                        {
                            Detail = "Invalid file url",
                            Status = 400
                        });
                    }
                }
                else if (file is not null)
                {
                    stream = file.OpenReadStream();
                }
                else
                {
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = "Either a file or a link is required",
                        Status = 400
                    });
                }

                manager.Print(id, stream, new PrintProfile
                {
                    Resolution = resolution,
                    PageSize = format,
                    Duplex = duplex
                });

                if (stream is not null)
                    stream.Dispose();

                _logger.LogDebug("Print request completed");

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Print request failed because of exception");

                if (ex is PrintException pex)
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = pex.Message,
                        Status = pex.StatusCode
                    });
                else
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = ex.Message,
                        Status = 500
                    });
            }
        }
    }
}
