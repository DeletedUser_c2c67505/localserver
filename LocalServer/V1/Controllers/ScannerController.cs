using Asp.Versioning;
using Core.Device;
using Microsoft.AspNetCore.Mvc;

namespace LocalServer.V1.Controllers
{
    /// <summary>
    /// Контроллер для работы со сканерами
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("device/v{version:apiVersion}/[controller]")]
    public class ScannerController : ControllerBase
    {
        private readonly ILogger<ScannerController> _logger;

        public ScannerController(ILogger<ScannerController> logger)
        {
            _logger = logger;
            _logger.LogDebug("NLog injected into ScannerController");
        }

        /// <summary>
        /// Список доступных TWAIN сканнеров.
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<DeviceInfo>), StatusCodes.Status200OK)]
        public virtual ActionResult<IEnumerable<DeviceInfo>> GetDevices()
        {
            _logger.LogDebug("GetDevices request recieved");

            try
            {
                using var manager = new ScannerManager();

                var devices = manager.GetDevices();

                _logger.LogDebug("GetDevices request completed");

                return Ok(devices);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "GetDevices request failed because of exception");

                if (ex is ScanException pex)
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = pex.Message,
                        Status = pex.StatusCode
                    });
                else
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = ex.Message,
                        Status = 500
                    });
            }
        }

        /// <summary>
        /// Информация о выбранном сканнере.
        /// </summary>
        /// <param name="id">Имя сканнера</param>
        [HttpGet]
        [ProducesResponseType(typeof(FullDeviceInfo), StatusCodes.Status200OK)]
        [Route("{id}")]
        public virtual ActionResult<FullDeviceInfo> GetDeviceInfo(string id)
        {
            _logger.LogDebug("GetDeviceInfo request recieved with id: " + id);

            try
            {
                using var manager = new ScannerManager();

                var device = manager.GetDeviceInfo(id);

                _logger.LogDebug("GetDeviceInfo request completed");

                return Ok(device);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "GetDeviceInfo request failed because of exception");

                if (ex is ScanException pex)
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = pex.Message,
                        Status = pex.StatusCode
                    });
                else
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = ex.Message,
                        Status = 500
                    });
            }
        }

        /// <summary>
        /// Запрос на сканирование выбранному сканнеру.
        /// </summary>
        /// <param name="id">Имя сканнера</param>
        /// <param name="type">Тип источника: Со стекла, Автоматическая подача или Двустороннее сканирование</param>
        /// <param name="pixel_type">Цветовая схема: Чёрнобелый, Оттенки серого или Цветной</param>
        /// <param name="format">Формат бумаги: A4, B3, Letter и тд</param>
        /// <param name="resolution">Разрешение сканирования: 75dpi, 200dpi и тд</param>
        /// <param name="bit_depth">Глубина цвета: от 1 до 48 бит</param>
        /// <param name="brightness">Яркость: от -1000.0 до +1000.0</param>
        /// <param name="contrast">Контраст: от -1000.0 до +1000.0</param>
        /// <param name="show_gui">Сканирование через пользовательский интерфейс (Если включено - игнорирует все предустановки)</param>
        [HttpGet]
        [Produces("application/pdf")]
        [ProducesResponseType(typeof(FileContentResult), StatusCodes.Status200OK)]
        [Route("{id}/scan_pages")]
        public virtual ActionResult ScanPages(ScanType? type, PixelType? pixel_type, PageSize? format,
             double? resolution, int? bit_depth, double? brightness, double? contrast, string id,
             bool? show_gui)
        {
            _logger.LogDebug("ScanPages request recieved with id: " + id);

            try
            {
                using var manager = new ScannerManager();

                var scan = manager.Scan(id, new ScanProfile
                {
                    ScanType = type,
                    PixelType = pixel_type,
                    PageSize = format,
                    Resolution = resolution,
                    BitDepth = bit_depth,
                    Brightness = brightness,
                    Contrast = contrast,
                    ShowGUI = show_gui,
                }, out string contentType);

                _logger.LogDebug("ScanPages request completed");

                return File(scan, contentType);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ScanPages request failed because of exception");

                if (ex is ScanException pex)
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = pex.Message,
                        Status = pex.StatusCode
                    });
                else
                    return new ObjectResult(new ProblemDetails
                    {
                        Detail = ex.Message,
                        Status = 500
                    });
            }
        }
    }
}
