# local-server

Для сборки проекта:
```
dotnet build .\LocalServer\LocalServer.csproj -a x86 -c release
```

Параметры запуска:
```
--debug или -d 
    Запуск приложения в режиме отладки, что включает в себя:
    1. Уровень логирования - DEBUG
    2. Акцивация страницы Swagger (/swagger)
    3. Дополнительный вывод всех логов сервера в консоль
```

## Функции сервера

Основная идея - предоставить доступ к локальным ресурсам из браузера. 
Вижу это так: на ПК пользователя запускаем http сервер на localhost:1234 (порт и адрес берем из конфига)
На сервере есть несколько эндпоинтов:

GET /device - вернуть список оборудования:
```json
{
    id: str,
    type: str
}

```

### для ошибок используем схему:
```json
{
    "status": int,
    "detail": str
}
```

GET /device/{type}/{id} вернуть конкретный девайс
```json
{
    "capabilities": {
        "duplex": bool,
        "adf": bool,
        "color": bool
    },
    "formats": {
        "format_name": {
            "width": float,
            "height": float,
            "unit": str
        }
    },
    "id": str,
    "type": str
}
```

## Принтеры

### POST /print передать страницу на печать 
```http
POST /print?duplex=true&format=a4 HTTP/1.1
Host: localhost:5000
Content-Type: multipart/form-data
Content-Length: 284
```
Возможные параметры:  
```
format - Формат бумаги: a4, b3, letter и тд [ Опционально ]  
resolution - Разрешение печати: high, medium, low [ Опционально ]  
duplex - Двусторонняя печать: true, false [ Опционально ]

file_url - Ссылка на документ в формате PDF  
или  
file - массив байтов файла в формате PDF
```

## Сканеры

GET /scan_pages?type=duplex&pixel_type=grayscale

Возможные параметры:  
```
type - Тип источника: glass, feeder или duplex [ Опционально ]  
pixel_type - Цветовая схема: whiteblack, grayscale или color [ Опционально ]  
format - Формат бумаги: a4, b3, letter и тд [ Опционально ]  
resolution - Разрешение сканирования: 75, 200 и тд [ Опционально ]  
bit_depth - Глубина цвета: от 1 до 48 [ Опционально ]  
brightness - Яркость: от -1000.0 до +1000.0 [ Опционально ]  
contrast - Контраст: от -1000.0 до +1000.0 [ Опционально ]  
show_gui - Сканирование через пользовательский интерфейс (Если включено - игнорирует все предустановки) [ Опционально ]  
```
