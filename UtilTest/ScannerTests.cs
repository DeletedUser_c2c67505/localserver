using Core.Windows.Device;
using CoreBase.Device;

namespace UtilTest
{
    public class ScannerTests
    {
        private ScannerManager _scannerManager;

        [SetUp]
        public void Setup()
        {
            _scannerManager = new ScannerManager();
        }

        [Test]
        public void GetDevice_IsDevicesFound()
        {
            var devices = _scannerManager.GetDevices();

            Assert.That(devices.Count, Is.GreaterThan(0));
        }

        [Test]
        public void GetDeviceInfo_IsDeviceInfoIsRight()
        {
            DeviceInfo? device = _scannerManager.GetDeviceInfo("Pantum MS6550 Series TWAIN");

            Assert.IsNotNull(device, "Device not found");

            if (device is FullDeviceInfo info)
            {
                Assert.IsTrue(info.Capabilities["duplex"], "Duplex is not supported");
                Assert.IsTrue(info.Capabilities["adf"], "Automatic document feeder is not supported");

                Assert.IsTrue(info.Formats.ContainsKey(PageSize.A4.ToString()), "Format A4 is not supported");

                Assert.That(PageSize.A4.Dimensions(), Is.EqualTo(info.Formats[PageSize.A4.ToString()]), "Selected size for A4 fomat is not equal to the actual size for A4 format");
            }
            else
                Assert.Fail();

            Assert.Pass("Device info is not full");
        }
    }
}