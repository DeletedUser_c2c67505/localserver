#!/bin/sh
apt-get update
apt-get install -y wget

wget https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb -O /tmp/packages-microsoft-prod.deb
sudo dpkg -i /tmp/packages-microsoft-prod.deb
rm /tmp/packages-microsoft-prod.deb

apt-get update
apt-get install -y dotnet-runtime-6.0 aspnetcore-runtime-6.0
